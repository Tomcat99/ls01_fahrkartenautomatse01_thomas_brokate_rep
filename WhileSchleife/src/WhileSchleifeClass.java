import java.util.Scanner;
public class WhileSchleifeClass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("Geben Sie eine Integer-Zahl ein: ");
		int lim = sc.nextInt();
		int i = 0;
		while (lim > i) {
			System.out.print(i+" ");
			i++;
		}
		System.out.println("Ende");
	}

}
